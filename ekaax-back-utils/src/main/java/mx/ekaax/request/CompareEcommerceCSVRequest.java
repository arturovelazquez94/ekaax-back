package mx.ekaax.request;

import java.util.List;

import mx.ekaax.model.ModelProduct;

public class CompareEcommerceCSVRequest {

	List<ModelProduct> listProdCSV;

	List<String> listProdCSVString;

	List<ModelProduct> listProdEcomm;

	String[] idEcommerce;

	public CompareEcommerceCSVRequest(List<ModelProduct> listProdEcomm, String[] idEcommerce) {
		super();
		this.listProdEcomm = listProdEcomm;
		this.idEcommerce = idEcommerce;
	}

	public CompareEcommerceCSVRequest(List<ModelProduct> listProdCSV, List<ModelProduct> listProdEcomm,
			String[] idEcommerce) {
		super();
		this.listProdCSV = listProdCSV;
		this.listProdEcomm = listProdEcomm;
		this.idEcommerce = idEcommerce;
	}

	public CompareEcommerceCSVRequest() {
		super();
	}

	public List<ModelProduct> getListProdCSV() {
		return listProdCSV;
	}

	public void setListProdCSV(List<ModelProduct> listProdCSV) {
		this.listProdCSV = listProdCSV;
	}

	public List<ModelProduct> getListProdEcomm() {
		return listProdEcomm;
	}

	public void setListProdEcomm(List<ModelProduct> listProdEcomm) {
		this.listProdEcomm = listProdEcomm;
	}

	public String[] getIdEcommerce() {
		return idEcommerce;
	}

	public void setIdEcommerce(String[] idEcommerce) {
		this.idEcommerce = idEcommerce;
	}

	public List<String> getListProdCSVString() {
		return listProdCSVString;
	}

	public void setListProdCSVString(List<String> listProdCSVString) {
		this.listProdCSVString = listProdCSVString;
	}
	

}
