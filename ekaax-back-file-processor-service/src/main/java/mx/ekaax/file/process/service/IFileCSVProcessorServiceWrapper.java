package mx.ekaax.file.process.service;

import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;


public interface IFileCSVProcessorServiceWrapper {

	CompareFileEcommerceResponse processFile(MultipartFile file, String ecommerce);

}
