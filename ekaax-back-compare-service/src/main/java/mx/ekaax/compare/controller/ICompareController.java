package mx.ekaax.compare.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;

@RestController
@RequestMapping("/ekaax/api/compare")
public interface ICompareController {

	@GetMapping
	public ResponseEntity<?> consultTest();

	@PostMapping("/v1/product")
	public CompareModelsResponse compareModel(CompareModelsRequest modelProducts);
	
	@PostMapping("/v1/product/ecommerce")
	public CompareModelsResponse compareModelsEcom(CompareModelsRequest modelProductsRequest);

	@PostMapping("/v1/product/csv/ecommerce")
	public CompareFileEcommerceResponse compareCsvEcom(CompareEcommerceCSVRequest compareEcomCsvReq);

}
