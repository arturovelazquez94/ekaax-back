package mx.ekaax.compare.helper;

import java.util.ArrayList;
import java.util.List;

import mx.ekaax.compare.constants.CompareServiceConstants;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;

/**
 * 
 * @author arturo
 *
 */
public class CompareHelper {
	/**
	 * 
	 * @author arturo
	 *
	 */
	public static enum EcommerceProductsList {

		CSV("CSV"), ECOMMERCE("Ecommerce");

		private String type;

		private EcommerceProductsList(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

	}

	/**
	 * 
	 * This method validates if the list of products is not empty of null
	 * 
	 * @param listProducts
	 * @param productType
	 */
	public static void validateoProductsList(List<ModelProduct> listProducts, EcommerceProductsList productType) {

		if (Nullable.isNullOrEmpty(listProducts)) {
			// Se va a añadir un modulo de exepciones personalizadas, se lanza el
			// RuntimException de forma temporal
			String type = "";
			type = productType.getType();

			throw new RuntimeException("No se encontro informacion en el " + type);
		}
	}

	/**
	 * This method validates the existence of the product in the ecommerce, in such
	 * case the error is store in a list of errors to be displayed to the user
	 * 
	 * @param productErrors
	 * @param productPresent
	 * @param productsFoundEcommerce
	 */
	public static void ecommerceProductExistenceError(List<ProductEcommerceError> productErrors,
			ModelProduct productPresent, List<ModelProduct> productsFoundEcommerce) {

		ProductEcommerceError productError = new ProductEcommerceError();
		System.out.println(":::::::::::::::::::::::::::::Products:::::::::::::::::." + productsFoundEcommerce);
		System.out.println(":::::::::::::::::::::::::_::CONTAINS::::::::::::::::"
				+ productsFoundEcommerce.contains(productPresent));
		boolean present = productsFoundEcommerce.contains(productPresent);
		if (!present) {
			System.out.println("Entrando al error");
			productError.setSku(productPresent.getSkuId());
			productError.setMessage(CompareServiceConstants.PRODUCT_ECOMMERCE_NOT_EXISTS);

			productErrors.add(productError);
		}

		System.out.println(":::::::::::::::::Lista errores:::::::::::::::" + productErrors);

	}

	public static void addEcommerceProductExistenceError(List<ProductEcommerceError> productErrors,
			ModelProduct productPresent) {

		ProductEcommerceError productError = new ProductEcommerceError();
		
		productError.setSku(productPresent.getSkuId());
		productError.setMessage(CompareServiceConstants.PRODUCT_ECOMMERCE_NOT_EXISTS);
		productErrors.add(productError);


	}

}
