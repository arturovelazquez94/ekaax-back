package mx.ekaax.file.process.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.file.process.feignclient.consult.IFeignClientCompare;
import mx.ekaax.file.process.feignclient.consult.IFeignClientConsultEcommerce;
import mx.ekaax.file.process.service.IFileCSVProcessorService;
import mx.ekaax.file.process.service.IFileCSVProcessorServiceWrapper;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.FileCSVRowResponse;

@Service
public class FileCSVProcessorServiceWrapperImpl implements IFileCSVProcessorServiceWrapper {

	@Autowired
	private IFileCSVProcessorService fileProcessorService;

	@Autowired
	private IFeignClientConsultEcommerce feignClientConsultEcommerce;

	@Autowired
	private IFeignClientCompare feignClientCompare;

	@Autowired
	private ProductMapper productMapper;

	@Override
	public CompareFileEcommerceResponse processFile(MultipartFile file, String ecommerce) {

		CompareFileEcommerceResponse listProductsCSV = null;
		List<ModelProduct> listProductsCSVModel = null;

		List<FileCSVRowResponse> csvResponse = fileProcessorService.processFile(file);

		List<String> csvsku = csvResponse.stream().map(responseRow -> responseRow.getSku())
				.collect(Collectors.toList());

		System.out.println("AFTER FILE PROCESS:::" + csvResponse);
		System.out.println("SKUW:::" + csvsku);

		System.out.println("::::::::BEFORE MAPPER:::::::");

		listProductsCSVModel = productMapper.csvResponseToModel(csvResponse);

		System.out.println("::::::::MAPPER:::::::" + listProductsCSVModel);

		String[] ecommerceArray = { ecommerce };

		System.out.println("::::::AFTER MAPPER:::");

		CompareEcommerceCSVRequest request = new CompareEcommerceCSVRequest();
		request.setIdEcommerce(ecommerceArray);
		request.setListProdCSVString(csvsku);

		List<ModelProduct> ecommerceProducts = feignClientConsultEcommerce.consultEcommerce(request);

		System.out.println("::::::AFTER FETCH PRODUCTS ECOMMERCE:::" + ecommerceProducts);

		CompareEcommerceCSVRequest compareEcomCsvReq = new CompareEcommerceCSVRequest();
		compareEcomCsvReq.setListProdCSV(listProductsCSVModel);
		compareEcomCsvReq.setListProdEcomm(ecommerceProducts);

		System.out.println("CSV:::" + compareEcomCsvReq.getListProdCSV());
		System.out.println("ecommerce:::" + compareEcomCsvReq.getListProdEcomm());
		compareEcomCsvReq.setListProdCSVString(csvsku);

		listProductsCSV = feignClientCompare.compareProductsCsvEcommerce(compareEcomCsvReq);
		System.out.println("::::::AFTER COMPARE ECOMMERCE PRODUCTS:::");

		return listProductsCSV;
	}

}
