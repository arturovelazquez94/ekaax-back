package mx.ekaax.consult.ecommerce.api;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConsumeApi {
	
	public static ResponseEntity<String> consumeApiChedraui(String url) {
		
		try {
			CloseableHttpClient httpClient = HttpClients.custom()
			        .setSSLHostnameVerifier(new NoopHostnameVerifier())
			        	.build();
		    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		    requestFactory.setHttpClient(httpClient);
		    
		    ResponseEntity<String> response = new RestTemplate(requestFactory).exchange(url, HttpMethod.GET, null, String.class);
		    
		    return response;
		} catch (Exception e) {
			return null;
		}
	    
	}

}
