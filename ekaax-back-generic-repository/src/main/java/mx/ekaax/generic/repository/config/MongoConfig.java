package mx.ekaax.generic.repository.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories(basePackages = "mx.ekaax.generic")
public class MongoConfig extends AbstractMongoConfiguration {

	@Override
	public MongoClient mongoClient() {
		return new MongoClient("mongo", 27017);
		//return new MongoClient("localhost", 27024);
	}

	@Override
	protected String getDatabaseName() {
		return "ekaaxDB";
	}
	
	@Override
    protected String getMappingBasePackage() {
		return "mx.ekaax.generic.repository.entity";
    }

}
