package mx.ekaax.file.process.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.request.CompareEcommerceCSVRequest;


@RestController
@RequestMapping("/files")
public interface IFileCSVProcessorController {

	@PostMapping
	ResponseEntity<?> process(MultipartFile file, String ecomeerce);
	
	@GetMapping("/example")
	ResponseEntity<?> example();
}
