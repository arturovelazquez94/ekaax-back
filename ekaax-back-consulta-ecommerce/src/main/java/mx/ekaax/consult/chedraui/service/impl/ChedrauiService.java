package mx.ekaax.consult.chedraui.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import mx.ekaax.consult.chedraui.service.IChedrauiService;
import mx.ekaax.consult.ecommerce.api.ApiChedrauiUrl;
import mx.ekaax.consult.ecommerce.api.ConsumeApi;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class ChedrauiService implements IChedrauiService{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired 
	private ApiChedrauiUrl ecommerceUrl;
	
	@Override
	public ModelProduct consultProduct(String upcSku) {
        ObjectMapper mapper = new ObjectMapper();
        ModelProduct modelProduct;
		
        modelProduct = new ModelProduct();
		
		try {
			JsonNode rootJson = mapper.readTree(
					ConsumeApi.consumeApiChedraui(ecommerceUrl.getChedrauiApi1().replaceAll(":upcSku", upcSku)).getBody()
					);
			
			ArrayNode productJsonArray = (ArrayNode) rootJson.path("products");
			rootJson = productJsonArray.get(0);
			
			
			modelProduct.setEcommerce(EcommerceEnum.CHEDRAUI.getNameEcommerce());
			modelProduct.setUpcId(rootJson.path("sapEAN").asText());
			modelProduct.setSkuId(rootJson.path("sapEAN").asText());
			modelProduct.setTitle(rootJson.path("name").asText());
			modelProduct.setSummary(rootJson.path("summary").asText());
			modelProduct.setDescription(rootJson.path("description").asText());
			
			JsonNode precioJson = rootJson.path("price");
			modelProduct.setPrice(BigDecimal.valueOf(precioJson.path("value").asDouble()));
			
			ArrayNode imagesJsonArray = (ArrayNode) rootJson.path("images");
			modelProduct.setPicture("https://www.chedraui.com.mx"+imagesJsonArray.get(0).path("url").asText());
		} catch (Exception e) {
			logger.error("#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*");
			logger.error("START ERROR al recuperar el producto del ecommerce");
			logger.error(e);
			logger.error("ERROR END #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*#*#*#*##*#*#*#*#*#*#*");
			modelProduct = null;
		}
		
		return modelProduct;
		
	}

	@Override
	public void create(Product product) {
		try {
			//productRepository.insert(product);
		} catch (Exception ex) {
			logger.error(ex);
			throw ex;
		}
	}

	@Override
	public void update(Product product) {
		try {
			//productRepository.save(product);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<Product> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
