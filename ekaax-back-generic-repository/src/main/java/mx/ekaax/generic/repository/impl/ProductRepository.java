package mx.ekaax.generic.repository.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.entity.Product;

@Repository
public abstract class ProductRepository implements IProductRepository{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public Product findProductBySkuIdOrUpcId(String upcSku) {
		try {
//			Criteria criteria = new Criteria();
//	        criteria.orOperator(Criteria.where("skuId").is(upcSku),Criteria.where("upcId").is(upcSku));
//	        Query query = new Query(criteria);
//			return mongoTemplate.findOne(query, Product.class);
			Query query = new Query();
			query.addCriteria(Criteria.where("skuId").is(upcSku));
			Product prod = mongoTemplate.findOne(query, Product.class);
			return prod;
		} catch (Exception e) {
			logger.info("DAO: "+e);
			return null;
		}
	}
	
	public Product findProductBySkuId(String skuId) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("skuId").is(skuId));
			return mongoTemplate.findOne(query, Product.class);
		} catch (Exception e) {
			logger.info("DAO Exception: "+e);
			return null;
		}
	}

}
