package mx.ekaax.model;

import java.util.List;

public class ModelEcommerce {
	
	private String nameEcommerce;
	
	private List<String> apiEcommerce;

	public ModelEcommerce() {
		super();
	}

	public ModelEcommerce(String nameEcommerce, List<String> apiEcommerce) {
		super();
		this.nameEcommerce = nameEcommerce;
		this.apiEcommerce = apiEcommerce;
	}

	public String getNameEcommerce() {
		return nameEcommerce;
	}

	public void setNameEcommerce(String nameEcommerce) {
		this.nameEcommerce = nameEcommerce;
	}

	public List<String> getApiEcommerce() {
		return apiEcommerce;
	}

	public void setApiEcommerce(List<String> apiEcommerce) {
		this.apiEcommerce = apiEcommerce;
	}

	@Override
	public String toString() {
		return "ModelEcommerce [nameEcommerce=" + nameEcommerce + ", apiEcommerce=" + apiEcommerce + "]";
	}

	
}
