package mx.ekaax.consult.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;

/**
 * 
 * @author alfonso
 *
 */
@RestController
@RequestMapping("/ekaax/api")
public interface IEkaaxController {
	
	@GetMapping
	public ResponseEntity<?> consultTest();
	
	/**
	 * Method for consult one product in the database's ekaax
	 * 
	 * @param upcSku
	 * @return
	 */
	@GetMapping(value="/v1/consulta/{upcSku}")
	public ResponseEntity<?> consultProductEkaax(String upcSku);
	
	/**
	 * Method for consult one product from the database vs some ecommerce 
	 * 
	 * @param upcSku
	 * @param ecommerce
	 * @param compare flat to know if we are going 
	 * @return
	 */
	@PostMapping(value="/v1/consulta/{upcSku}")
	public ResponseEntity<?> consultProduct(String upcSku, Integer[] ecommerce, boolean compare);

	@PostMapping(value="/v1/consulta")
	public List<ModelProduct> csvConsultEcommController(@RequestBody CompareEcommerceCSVRequest request);
	
}
