package mx.ekaax.response;

public class ProductEcommerceError {

	private String sku;

	private String message;

	public ProductEcommerceError() {
		super();
	}

	
	
	public ProductEcommerceError(String sku, String message) {
		super();
		this.sku = sku;
		this.message = message;
	}



	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
