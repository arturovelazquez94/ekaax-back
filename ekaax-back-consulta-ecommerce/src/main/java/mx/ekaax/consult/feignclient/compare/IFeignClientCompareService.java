package mx.ekaax.consult.feignclient.compare;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareModelsResponse;

@FeignClient(name="gateway")
public interface IFeignClientCompareService {

	@PostMapping("ekaax/compare-products/ekaax/api/compare/v1/product")
	public CompareModelsResponse compareModel(@RequestBody CompareModelsRequest compareModelRequest);
	
	@PostMapping("ekaax/compare-products/ekaax/api/compare/v1/product/ecommerce")
	public CompareModelsResponse compareModelsEcom(@RequestBody CompareModelsRequest compareModelRequest);
	
}
