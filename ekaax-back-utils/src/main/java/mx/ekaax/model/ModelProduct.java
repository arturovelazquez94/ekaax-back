package mx.ekaax.model;

import java.math.BigDecimal;

public class ModelProduct {

	private String ecommerce;

	private String skuId;

	private String upcId;

	private String title;

	private String summary;

	private String description;

	private BigDecimal minPrice;

	private BigDecimal price;

	private BigDecimal maxPrice;

	private String picture;

	private String promotion;

	private boolean bTitle;

	private boolean bDescription;

	private boolean bBasePrice;

	public ModelProduct() {
		super();
	}

	public ModelProduct(String ecommerce, String skuId, String upcId, String title, String summary, String description,
			BigDecimal minPrice, BigDecimal price, BigDecimal maxPrice, String picture, String promotion,
			boolean bTitle, boolean bDescription, boolean bBasePrice) {
		super();
		this.ecommerce = ecommerce;
		this.skuId = skuId;
		this.upcId = upcId;
		this.title = title;
		this.summary = summary;
		this.description = description;
		this.minPrice = minPrice;
		this.price = price;
		this.maxPrice = maxPrice;
		this.picture = picture;
		this.promotion = promotion;
		this.bTitle = bTitle;
		this.bDescription = bDescription;
		this.bBasePrice = bBasePrice;
	}

	public ModelProduct(String ecommerce, String skuId, String upcId, String title, String summary, String description,
			BigDecimal minPrice, BigDecimal price, BigDecimal maxPrice, String picture, String promotion) {
		super();
		this.ecommerce = ecommerce;
		this.skuId = skuId;
		this.upcId = upcId;
		this.title = title;
		this.summary = summary;
		this.description = description;
		this.minPrice = minPrice;
		this.price = price;
		this.maxPrice = maxPrice;
		this.picture = picture;
		this.promotion = promotion;
	}

	public String getEcommerce() {
		return ecommerce;
	}

	public void setEcommerce(String ecommerce) {
		this.ecommerce = ecommerce;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getUpcId() {
		return upcId;
	}

	public void setUpcId(String upcId) {
		this.upcId = upcId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public boolean isbTitle() {
		return bTitle;
	}

	public void setbTitle(boolean bTitle) {
		this.bTitle = bTitle;
	}

	public boolean isbDescription() {
		return bDescription;
	}

	public void setbDescription(boolean bDescription) {
		this.bDescription = bDescription;
	}

	public boolean isbBasePrice() {
		return bBasePrice;
	}

	public void setbBasePrice(boolean bBasePrice) {
		this.bBasePrice = bBasePrice;
	}

	@Override
	public String toString() {
		return "ModelProduct [ecommerce=" + ecommerce + ", skuId=" + skuId + ", upcId=" + upcId + ", title=" + title
				+ ", summary=" + summary + ", description=" + description + ", minPrice=" + minPrice + ", price="
				+ price + ", maxPrice=" + maxPrice + ", picture=" + picture + ", promotion=" + promotion + ", bTitle="
				+ bTitle + ", bDescription=" + bDescription + ", bBasePrice=" + bBasePrice + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((skuId == null) ? 0 : skuId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelProduct other = (ModelProduct) obj;
		if (skuId == null) {
			if (other.skuId != null)
				return false;
		} else if (!skuId.equals(other.skuId))
			return false;
		return true;
	}

}
