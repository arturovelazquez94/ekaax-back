package mx.ekaax.consult.service;

import java.util.List;

import mx.ekaax.model.ModelProduct;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ConsultEcommerceResponse;

public interface IConsultService {
	
	/**
	 * Method for consult one product in the database's ekaax
	 * 
	 * @param upcSku
	 * @return
	 */
	public ConsultEcommerceResponse consultProductEkaax(String upcSku);
	
	/**
	 * Method for Compare one product in dataBase's ekaax VS Ecommerce
	 * @param upcSku
	 * @param ecommerce
	 * @return
	 */
	public ConsultEcommerceResponse compareDatabaseEcommerce(String upcSku, Integer[] ecommerce);
	
	/**
	 * Method for Compare One Product in an Ecommerce vs another ecommerce
	 * @param upcSku
	 * @param ecommerce
	 * @return
	 */
	public ConsultEcommerceResponse compareEcommerce(String upcSku, Integer[] ecommerce);
	
	public List<ModelProduct> csvConsultEcommService(List<String> listProdCSVString,  String[] ecommerce);
	
}
