package mx.ekaax.response;

import java.util.ArrayList;
import java.util.List;

import mx.ekaax.model.ModelProduct;

public class ConsultEcommerceResponse {

	private List<ModelProduct> modelProducts;

	private List<ProductEcommerceError> productErrors;

	public List<ModelProduct> getModelProducts() {
		return modelProducts;
	}

	public void setModelProducts(List<ModelProduct> modelProducts) {
		this.modelProducts = modelProducts;
	}

	public List<ProductEcommerceError> getProductErrors() {
		return productErrors;
	}

	public void setProductErrors(List<ProductEcommerceError> productErrors) {
		this.productErrors = productErrors;
	}

	public boolean addListModelProduct(ModelProduct e) {
		if(modelProducts == null) {
			modelProducts = new ArrayList<>();
		}
		return modelProducts.add(e);
	}

	public boolean addListProductEcommerceError(ProductEcommerceError e) {
		if(productErrors == null) {
			productErrors = new ArrayList<>();
		}
		return productErrors.add(e);
	}
	
	
	
}
