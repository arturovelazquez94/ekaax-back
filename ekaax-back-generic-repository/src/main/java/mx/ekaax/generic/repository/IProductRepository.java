package mx.ekaax.generic.repository;

import mx.ekaax.generic.repository.entity.Product;

public interface IProductRepository extends IMongoRepositoryProduct{
	
	public Product findProductBySkuIdOrUpcId(String upcSku);
	
	public Product findProductBySkuId(String skuId);

}
