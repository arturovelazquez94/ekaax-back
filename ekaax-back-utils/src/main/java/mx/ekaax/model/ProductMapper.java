package mx.ekaax.model;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;
import mx.ekaax.response.FileCSVRowResponse;

@Mapper(componentModel = "spring")
public interface ProductMapper {

	ModelProduct entityToModel(Product product);

	Product modelToEntity(ModelProduct model);
	
	ProductEcommerce modelEcommToEntity(ModelProduct model);

	@Mappings({ @Mapping(source = "fileResponse.sku", target = "skuId"),
			@Mapping(source = "fileResponse.upc", target = "upcId"),
			@Mapping(source = "fileResponse.titulo", target = "title"),
			@Mapping(source = "fileResponse.descripcion", target = "description"),
			@Mapping(source = "fileResponse.minPrice", target = "minPrice"),
			@Mapping(source = "fileResponse.maxPrice", target = "maxPrice"),
			@Mapping(source = "fileResponse.image", target = "picture") })
	ModelProduct csvResponseToModel(FileCSVRowResponse fileResponse);

	List<ModelProduct> csvResponseToModel(List<FileCSVRowResponse> fileResponse);

}
