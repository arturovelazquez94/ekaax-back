package mx.ekaax.file.process.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mx.ekaax.file.process.helper.FileProcessorHelper;
import mx.ekaax.file.process.service.IFileCSVProcessorService;
import mx.ekaax.response.FileCSVRowResponse;
import mx.ekaax.utils.general.Nullable;

@Service
public class FileProcessorServiceImpl implements IFileCSVProcessorService {

	@Override
	public List<FileCSVRowResponse> processFile(MultipartFile file) {
		// TODO Auto-generated method stub
		List<FileCSVRowResponse> rowsResponse = null;

		String classpath = System.getProperty("java.class.path");
		Path classPathFile = Paths.get(classpath);

		if (Nullable.isNotNull(file)) {
			Path fileCSV = Paths.get(classPathFile.getParent() + "/static/file.csv");
			try {

				if (!Files.exists(fileCSV)) {
					Files.createDirectories(fileCSV.getParent());
					Files.createFile(fileCSV);
				}
				Files.copy(file.getInputStream(), fileCSV, StandardCopyOption.REPLACE_EXISTING);
				// Processing CSV File and generating output Response
				rowsResponse = FileProcessorHelper.generateCSVOutput(fileCSV);

			} catch (IOException e) {
				// Se va a añadir un modulo de exepciones personalizadas, se lanza el
				// RuntimException de forma temporal
				e.printStackTrace();
				throw new RuntimeException("Hubo un error cargando el archivo:::", e);
			}

		} else {
			// Se va a añadir un modulo de exepciones personalizadas, se lanza el
			// RuntimException de forma temporal
			throw new RuntimeException("Archivo no se cargó correctamente");
		}

		return rowsResponse;

	}

}
