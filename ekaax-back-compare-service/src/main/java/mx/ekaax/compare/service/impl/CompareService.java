package mx.ekaax.compare.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ekaax.compare.service.ICompareService;
import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;
import static mx.ekaax.compare.helper.CompareHelper.*;

@Service
public class CompareService implements ICompareService {
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	private ModelProduct productBase;
	private ModelProduct productCompare;

	@Override
	public CompareModelsResponse compareProductDB(CompareModelsRequest modelProductsRequest) {
		CompareModelsResponse compareModelResponse = new CompareModelsResponse();
		logger.info("COMPARE MODEL START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		
		productBase = modelProductsRequest.getModelBase();
		productCompare = modelProductsRequest.getModelCompare();
		
		compareProductWithDatabase();
		
		compareModelResponse.setModelBase(productBase);
		compareModelResponse.setModelCompare(productCompare);
		
		logger.info("COMPARE MODEL END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");

		return compareModelResponse;

	}

	@Override
	public CompareModelsResponse compareProductsEcom(CompareModelsRequest modelProductsRequest) {
		CompareModelsResponse compareModelResponse = new CompareModelsResponse();

		productBase = modelProductsRequest.getModelBase();
		productCompare = modelProductsRequest.getModelCompare();
		
		compareProductsEcommerce();

		compareModelResponse.setModelBase(productBase);
		compareModelResponse.setModelCompare(productCompare);

		return compareModelResponse;
	}

	@Override
	public CompareFileEcommerceResponse compareCsvEcom(List<ModelProduct> listProdCSV,
			List<ModelProduct> listProdEcomm) {
		logger.info(
				"compareCsvEcom START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");

		logger.info("List products CSV service::::::::::::::::::::::::" + listProdCSV);
		logger.info("List products ECOMMERCE service::::::::::::::::::::::::" + listProdEcomm);

		CompareFileEcommerceResponse compareFileResponse = new CompareFileEcommerceResponse();
		List<ModelProduct> modelProducts = new ArrayList<>();
		List<ProductEcommerceError> productErrors = new ArrayList<>();

		// Data list validation
		validateoProductsList(listProdCSV, EcommerceProductsList.CSV);
		validateoProductsList(listProdEcomm, EcommerceProductsList.ECOMMERCE);

		listProdCSV.stream().forEach(ecommerceProductCorrect -> {

			// Validate existance of the product in the file
			ecommerceProductExistenceError(productErrors, ecommerceProductCorrect, listProdEcomm);

			listProdEcomm.stream().forEach(ecommerceProductCompare -> {

				logger.info("PRODUCT CSV service::::::::::::::::::::::::" + ecommerceProductCorrect);
				logger.info("PRODUCT service::::::::::::::::::::::::" + ecommerceProductCompare);
				if (Nullable.isNotNull(ecommerceProductCompare)) {
					String skuProductCorrect = ecommerceProductCorrect.getSkuId();
					String skStringCompare = ecommerceProductCompare.getSkuId();
					if (skuProductCorrect.equals(skStringCompare)) {

						// Title
						String correctTitle = ecommerceProductCorrect.getTitle();
						String ecommerceTitle = ecommerceProductCompare.getTitle();

						if (!correctTitle.equals(ecommerceTitle)) {
							ecommerceProductCompare.setbTitle(false);
						}

						// Price
						BigDecimal correctMaxPrice = ecommerceProductCorrect.getMaxPrice();
						BigDecimal correctMinPrice = ecommerceProductCorrect.getMinPrice();

						BigDecimal ecommercePrice = ecommerceProductCompare.getPrice();
						if (ecommercePrice.compareTo(correctMaxPrice) == 1
								&& ecommercePrice.compareTo(correctMinPrice) == -1) {

							ecommerceProductCompare.setbBasePrice(false);
						}

						// Description
						String correctDescription = ecommerceProductCorrect.getDescription();
						String ecommerceDescription = ecommerceProductCompare.getDescription();

						if (!correctDescription.equals(ecommerceDescription)) {
							ecommerceProductCompare.setbDescription(false);

						}
						modelProducts.add(ecommerceProductCompare);
					}
				}

			});

		});
		System.out.println("Errors:::::::::" + productErrors);

		compareFileResponse.setModelProducts(modelProducts);
		compareFileResponse.setProductErrors(productErrors);
		System.out.println("Product ERRORS::::" + compareFileResponse);

		return compareFileResponse;
	}

	public void compareProductWithDatabase() {
		// ============== COMPARACION ============== (VALIDAR CON PREDICADOS DE JAVA 8 )
		if (productCompare.getTitle() != null && productCompare.getTitle().equals(productBase.getTitle())) {
			productCompare.setbTitle(true);
		} else {
			productCompare.setbTitle(false);
		}

		if (productCompare.getDescription() != null
				&& productCompare.getDescription().equals(productBase.getDescription())) {
			productCompare.setbDescription(true);
		} else {
			productCompare.setbDescription(false);
		}

		BigDecimal correctMaxPrice = productBase.getMaxPrice();
		BigDecimal correctMinPrice = productBase.getMinPrice();
		if (productCompare.getPrice().compareTo(correctMaxPrice) == 1
				&& productCompare.getPrice().compareTo(correctMinPrice) == -1) {
			productCompare.setbBasePrice(false);
		} else {
			productCompare.setbBasePrice(true);
		}
	}
	
	public void compareProductsEcommerce() {
		// Title
		String correctTitle = productBase.getTitle();
		String ecommerceTitle = productCompare.getTitle();

		if (!correctTitle.equals(ecommerceTitle)) {
			productCompare.setbTitle(false);
		}
		// Price
		BigDecimal correctPrice = productBase.getPrice();
		BigDecimal ecommercePrice = productCompare.getPrice();

		if (correctPrice.compareTo(ecommercePrice) == 0) {
			productCompare.setbBasePrice(false);
		}

		// Description
		String correctDescription = productBase.getDescription();
		String ecommerceDescription = productCompare.getDescription();

		if (!correctDescription.equals(ecommerceDescription)) {
			productCompare.setbDescription(false);
		}
	}

}
