package mx.ekaax.response;

import mx.ekaax.model.ModelProduct;

public class CompareModelsResponse {

	ModelProduct modelBase;

	ModelProduct modelCompare;

	public CompareModelsResponse() {
		super();
	}

	public CompareModelsResponse(ModelProduct modelBase, ModelProduct modelCompare) {
		super();
		this.modelBase = modelBase;
		this.modelCompare = modelCompare;
	}

	public ModelProduct getModelBase() {
		return modelBase;
	}

	public void setModelBase(ModelProduct modelBase) {
		this.modelBase = modelBase;
	}

	public ModelProduct getModelCompare() {
		return modelCompare;
	}

	public void setModelCompare(ModelProduct modelCompare) {
		this.modelCompare = modelCompare;
	}

	@Override
	public String toString() {
		return "CompareModelsResponse [modelBase=" + modelBase + ", modelCompare=" + modelCompare + "]";
	}

}
