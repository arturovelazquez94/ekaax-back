package mx.ekaax.file.process.helper;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import mx.ekaax.response.FileCSVRowResponse;
import mx.ekaax.utils.general.Nullable;

public class FileProcessorHelper {

	public static List<FileCSVRowResponse> generateCSVOutput(Path fileCSV) {

		List<FileCSVRowResponse> rowObjects = null;

		try (Reader reader = Files.newBufferedReader(fileCSV);
				CSVReader csvReader = new CSVReaderBuilder(reader).build()) {
			// Reading Records One by One in a String array
			String header[] = csvReader.readNext();
			String[] nextRecord;

			rowObjects = new ArrayList<>();

			FileCSVRowResponse rowObject = null;
			int rowLine = 0;
			while ((nextRecord = csvReader.readNext()) != null) {

				rowLine++;
				validateCSVRow(header, nextRecord, rowLine);

				rowObject = generateRowObjectResponse(nextRecord);
				rowObjects.add(rowObject);
			}
		} catch (IOException e) {
			// Se va a añadir un modulo de exepciones personalizadas, se lanza el
			// RuntimException de forma temporal
			throw new RuntimeException("Hubo un error procesando el archivo CSV" + e.getMessage());

		} catch (Exception e) {
			// Se va a añadir un modulo de exepciones personalizadas, se lanza el
			// RuntimException de forma temporal
			throw new RuntimeException("Hubo un error leyendo la informacion del CSV" + e.getMessage());
		}

		return rowObjects;

	}

	public static void validateCSVRow(String[] header, String[] row, int rowLine) {

		StringBuilder messageRowError = new StringBuilder(" Empty columns [");
		Boolean flagError = false;

		for (int i = 0; i < row.length; i++) {

			if (Nullable.isNullOrWhitespace(row[i])) {
				messageRowError.append(header[i] + ",");
				flagError = true;
			}

		}
		messageRowError.append("]");
		messageRowError.append(" line: " + rowLine);

		if (flagError.equals(true)) {
			throw new RuntimeException(messageRowError.toString());
		}

	}

	public static FileCSVRowResponse generateRowObjectResponse(String[] row) {

		FileCSVRowResponse rowResponse = new FileCSVRowResponse();
		rowResponse.setSku(row[0]);
		rowResponse.setUpc(row[1]);
		rowResponse.setTitulo(row[2]);
		rowResponse.setDescripcion(row[3]);
		rowResponse.setMinPrice(Double.valueOf(row[4]));
		rowResponse.setMaxPrice(Double.valueOf(row[5]));
		rowResponse.setImage(row[6]);

		return rowResponse;
	}

}
