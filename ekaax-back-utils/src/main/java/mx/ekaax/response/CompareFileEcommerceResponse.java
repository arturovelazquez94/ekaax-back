package mx.ekaax.response;

import java.util.List;

import mx.ekaax.model.ModelProduct;


public class CompareFileEcommerceResponse {

	private List<ModelProduct> modelProducts;

	private List<ProductEcommerceError> productErrors;

	public List<ModelProduct> getModelProducts() {
		return modelProducts;
	}

	public void setModelProducts(List<ModelProduct> modelProducts) {
		this.modelProducts = modelProducts;
	}

	public List<ProductEcommerceError> getProductErrors() {
		return productErrors;
	}

	public void setProductErrors(List<ProductEcommerceError> productErrors) {
		this.productErrors = productErrors;
	}

}
