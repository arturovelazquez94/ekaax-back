package mx.ekaax.consult.controller.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import mx.ekaax.consult.controller.IEkaaxController;
import mx.ekaax.consult.service.IConsultService;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.ControllerHelper;

@Component
public class EkaaxController implements IEkaaxController {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IConsultService consultService;

	@Override
	public ResponseEntity<?> consultProductEkaax(@PathVariable(value = "upcSku", required = true) String upcSku) {
		logger.info("EkaaxController consultProduct >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		ConsultEcommerceResponse consultEcommResponse = consultService.consultProductEkaax(upcSku);
		
		if(Nullable.isNotNull(consultEcommResponse.getModelProducts())) {
			return ControllerHelper.responseOK(consultEcommResponse);
		}else {
			return ControllerHelper.responseNotFound(consultEcommResponse);
		}
	}

	@Override
	public ResponseEntity<?> consultProduct(@PathVariable(value = "upcSku", required = true) String upcSku,
			@RequestParam(value = "ecommerce", required = true) Integer[] ecommerce,
			@RequestParam(value = "compare", required = false) boolean compare) {
		
			logger.info("EkaaxController >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
			ConsultEcommerceResponse consultEcommerceResponse;
	
			if (compare) {
				consultEcommerceResponse = consultService.compareDatabaseEcommerce(upcSku, ecommerce);
				return ControllerHelper.responseOK(consultEcommerceResponse);
			} else {
				consultEcommerceResponse = consultService.compareEcommerce(upcSku, ecommerce);
				return new ResponseEntity<ConsultEcommerceResponse>(consultEcommerceResponse, HttpStatus.OK);
			}
			
	}

	@Override
	public List<ModelProduct> csvConsultEcommController(@RequestBody CompareEcommerceCSVRequest request) {
		System.out.println("-------Entrando a la busqueda---------");
		List<ModelProduct> models = consultService.csvConsultEcommService(request.getListProdCSVString(),
				request.getIdEcommerce());
		logger.info("Controller busqueda::::::::" + models);
		return models;
	}

	@Override
	public ResponseEntity<?> consultTest() {
		return new ResponseEntity<String>("Test Consult Ecommerce", HttpStatus.OK);
	}

}
