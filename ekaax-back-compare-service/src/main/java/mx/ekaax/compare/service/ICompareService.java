package mx.ekaax.compare.service;

import java.util.List;

import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;

public interface ICompareService {

//	public ModelProduct findByUpcSku(String upcSku) throws Exception;

	public CompareModelsResponse compareProductDB(CompareModelsRequest modelProductsRequest);
	
	public CompareModelsResponse compareProductsEcom(CompareModelsRequest modelProductsRequest);

	public CompareFileEcommerceResponse compareCsvEcom(List<ModelProduct> listProdCSV, List<ModelProduct> listProdEcomm);

}
