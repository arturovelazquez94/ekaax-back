package mx.ekaax.compare.controller.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import mx.ekaax.compare.controller.ICompareController;
import mx.ekaax.compare.service.ICompareService;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.request.CompareEcommerceCSVRequest;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareFileEcommerceResponse;
import mx.ekaax.response.CompareModelsResponse;

@Component
public class CompareController implements ICompareController{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private ICompareService compareService;

	@Override
	public CompareModelsResponse compareModel(@RequestBody CompareModelsRequest modelProductsRequest) {
		try {
			return compareService.compareProductDB(modelProductsRequest);
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	@Override
	public CompareModelsResponse compareModelsEcom(@RequestBody CompareModelsRequest modelProductsRequest) {
		logger.info(":::::::::::::::::::::::::::::::::::::::::::::::: CompareController compareModels :::::::::::::::::::::::::::::::::::::::::::::::: ");
		logger.info(modelProductsRequest);
		return compareService.compareProductsEcom(modelProductsRequest);
	}
	
	@Override
	public CompareFileEcommerceResponse compareCsvEcom(@RequestBody CompareEcommerceCSVRequest compareEcomCsvReq) {
		
		
		System.out.println("COMPAREC CSV:::"+compareEcomCsvReq.getListProdCSV());
		System.out.println("COMPARE ecommerce:::"+compareEcomCsvReq.getListProdEcomm());

		try {
			return compareService.compareCsvEcom(compareEcomCsvReq.getListProdCSV(), compareEcomCsvReq.getListProdEcomm());
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}
	
	@Override
	public ResponseEntity<?> consultTest() {
		return new ResponseEntity<String>("Test Consult Compare", HttpStatus.OK);
	}
	
}
