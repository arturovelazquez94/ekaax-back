package mx.ekaax.consult.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ekaax.consult.chedraui.service.IChedrauiService;
import mx.ekaax.consult.feignclient.compare.IFeignClientCompareService;
import mx.ekaax.consult.service.IConsultService;
import mx.ekaax.consult.walmart.service.IWalmartService;
import mx.ekaax.generic.repository.IProductEcommerceRepository;
import mx.ekaax.generic.repository.IProductRepository;
import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.generic.repository.entity.ProductEcommerce;
import mx.ekaax.model.ModelProduct;
import mx.ekaax.model.ProductMapper;
import mx.ekaax.request.CompareModelsRequest;
import mx.ekaax.response.CompareModelsResponse;
import mx.ekaax.response.ConsultEcommerceResponse;
import mx.ekaax.response.ProductEcommerceError;
import mx.ekaax.utils.general.Nullable;
import mx.ekaax.utils.general.helper.EcommerceEnum;

@Service
public class ConsultServiceImpl implements IConsultService {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IChedrauiService chedrauiService;

	@Autowired
	private IWalmartService walmartService;

	@Autowired
	private IFeignClientCompareService feignClientCompareService;

	@Autowired
	private IProductRepository productRepository;
	
	@Autowired
	private IProductEcommerceRepository productEcommerceRepository;

	@Autowired
	private ProductMapper productMapper;

	/**
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::: Consult Product in DataBase :::::::::::::::::::::
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ConsultEcommerceResponse consultProductEkaax(String upcSku) {
		ModelProduct model;
		ConsultEcommerceResponse consultEcommResp = new ConsultEcommerceResponse();
		ProductEcommerceError productEcommerceError = null;
		
		Product product = productRepository.findProductBySkuId(upcSku);
		if(Nullable.isNotNull(product)) {
			model = productMapper.entityToModel(product);
			consultEcommResp.addListModelProduct(model);
		}else {
			productEcommerceError = new ProductEcommerceError();
			productEcommerceError.setSku(upcSku);
			productEcommerceError.setMessage("No Encontrado en la BD de ekaax");
			consultEcommResp.addListProductEcommerceError(productEcommerceError);
		}
		return consultEcommResp;
	}

	/**
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::: Compare DataBase VS Ecommerce :::::::::::::
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ConsultEcommerceResponse compareDatabaseEcommerce(String upcSku, Integer[] ecommerce) {
		logger.info("START ConsultServiceImpl DataBase VS Ecommerce  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		List<ModelProduct> listProduct = new ArrayList<>();
		ModelProduct modelProdEcom = null;
		ModelProduct modelDBekaax = null;
		CompareModelsRequest compareModelRequest = new CompareModelsRequest(); 
		ConsultEcommerceResponse consultEcommResp = new ConsultEcommerceResponse();
		ProductEcommerceError productError = null;

		logger.info(":::::::::::::::::::::::::::::::::::::: BEFORE MONGO ::::::::::::::::::::::::::::::::::::: ");
		modelDBekaax = productMapper.entityToModel(productRepository.findProductBySkuId(upcSku));
		logger.info(":::::::::::::::::::::::::::::::::::::: AFTER MONGO :::::::::::::::::::::::::::::::::::::: ");
		logger.info(":::: " + modelDBekaax + " ::::");
		
		if (Nullable.isNotNull(modelDBekaax)) {
			modelDBekaax.setEcommerce("ekaax");
			listProduct.add(modelDBekaax);
			
			for (Integer tienda : ecommerce) {
				logger.info("::::::::::::::::::::: Tienda: " + tienda + ":::::::::::::::::::::::::::");
				try {
					if (tienda.equals(EcommerceEnum.CHEDRAUI.getIdEcommerce())) { // Chedraui = 1
						logger.info("CHEDRAUI ---------------------------------------" + upcSku);
						modelProdEcom = chedrauiService.consultProduct(upcSku);
					} else if (tienda.equals(EcommerceEnum.WALMART.getIdEcommerce())) { // Walmar = 2
						logger.info("WALMART ---------------------------------------" + upcSku);
						modelProdEcom = walmartService.consultProduct(upcSku);
					}
					
					if(Nullable.isNotNull(modelProdEcom)) {
						compareModelRequest.setModelBase(modelDBekaax);
						compareModelRequest.setModelCompare(modelProdEcom);
						modelProdEcom = feignClientCompareService.compareModel(compareModelRequest).getModelCompare();
						logger.info(modelProdEcom);
						listProduct.add(modelProdEcom);
					}else {
						productError = new ProductEcommerceError();
						productError.setSku(upcSku);
						productError.setMessage("Producto no encontrado en el ecommerce");
						consultEcommResp.addListProductEcommerceError(productError);
					}
				} catch (Exception e) {
					logger.error("ERROR Compare Compare DataBase VS Ecommerce --------------------------------------------");
					logger.error(e);
				}
			} //End For	
			consultEcommResp.setModelProducts(listProduct);
			
		} else {
			//Save product's ecommerce
			ProductEcommerce prodEcomEntity;
			
			modelProdEcom = chedrauiService.consultProduct(upcSku);
			if(Nullable.isNotNull(modelProdEcom)) {
				prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
				productEcommerceRepository.save(prodEcomEntity);
				modelProdEcom=null;
			}
			modelProdEcom = walmartService.consultProduct(upcSku);
			if(Nullable.isNotNull(modelProdEcom)) {
				prodEcomEntity = productMapper.modelEcommToEntity(modelProdEcom);
				productEcommerceRepository.insert(prodEcomEntity);
			}
			
			//send message error because we dont have product record in ekaax's database
			productError = new ProductEcommerceError();
			productError.setSku(upcSku);
			productError.setMessage("No Encontrado en la BD de ekaax");
			consultEcommResp.addListProductEcommerceError(productError);
			
		}
		
		logger.info("END ConsultServiceImpl DataBase VS Ecommerce  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");
		return consultEcommResp;
	}

	/**
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * :::::::::::::::::::::::::::: Compare Ecommerce VS Ecommerce ::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public ConsultEcommerceResponse compareEcommerce(String upcSku, Integer[] ecommerce) {
		logger.info(
				":::::::::::::::: ConsultServiceImpl -> compareEcommerce Compare Ecommerce VS Ecommerce :::::::::::::::::::::::::::::::::::::::::::::::");
		ModelProduct modelProdEcom1 = null;
		ModelProduct modelProdEcom2 = null;
		CompareModelsRequest requestCompareModel = null;
		CompareModelsResponse responseCompareModels = null;
		ConsultEcommerceResponse consultEcommerceResponse = new ConsultEcommerceResponse();
		try {
			if ( ecommerce[0].equals(EcommerceEnum.CHEDRAUI.getIdEcommerce()) ) {
				logger.info("CHEDRAUI ---------------------------------------" + upcSku);
				modelProdEcom1 = chedrauiService.consultProduct(upcSku);
				logger.info("WALMART ---------------------------------------" + upcSku);
				modelProdEcom2 = walmartService.consultProduct(upcSku);
			} else if ( ecommerce[1].equals(EcommerceEnum.WALMART.getIdEcommerce()) ) { 
				logger.info("WALMART ---------------------------------------" + upcSku);
				modelProdEcom1 = walmartService.consultProduct(upcSku);
				logger.info("CHEDRAUI ---------------------------------------" + upcSku);
				modelProdEcom2 = chedrauiService.consultProduct(upcSku);
			}

			if (modelProdEcom1 != null && modelProdEcom2 != null) {
				
				requestCompareModel = new CompareModelsRequest(modelProdEcom1, modelProdEcom2);
				responseCompareModels = feignClientCompareService.compareModelsEcom(requestCompareModel);
				consultEcommerceResponse.addListModelProduct(responseCompareModels.getModelBase());
				consultEcommerceResponse.addListModelProduct(responseCompareModels.getModelCompare());
				
			} else if(modelProdEcom1 == null && modelProdEcom2 != null){
				consultEcommerceResponse.addListModelProduct(modelProdEcom2);
				consultEcommerceResponse.addListProductEcommerceError(new ProductEcommerceError(upcSku, "Producto no encontrado en el ecommerce"));
			} else if(modelProdEcom2 == null && modelProdEcom1 != null) {
				consultEcommerceResponse.addListModelProduct(modelProdEcom1);
				consultEcommerceResponse.addListProductEcommerceError(new ProductEcommerceError(upcSku, "Producto no encontrado en el ecommerce"));
			}
		} catch (Exception e) {
			logger.error(
					"ERROR Compare Ecommerce VS Ecommerce ------------------------------------------------------------------");
			logger.error(e);
			logger.error(
					"ERROR Compare Ecommerce VS Ecommerce -----------------------------------------------------------");
		}

		return consultEcommerceResponse;
	}

	/**
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::: Compare CSV VS Ecommerce
	 * :::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	@Override
	public List<ModelProduct> csvConsultEcommService(List<String> listProdCSVString, String[] ecommerce) {
		logger.info("::::::ENTRANDO al csvConsultEcommService::::::");
		List<ModelProduct> listProdEcomm = new ArrayList<>();
		ModelProduct modelProdEcom = null;
		try {
			for (String upcSku : listProdCSVString) {
				logger.info(":SKU:::" + upcSku);

				if (ecommerce[0].equals("1")) { // Chedraui = 1
					logger.info("CHEDRAUI ---------------------------------------" + upcSku);
					modelProdEcom = chedrauiService.consultProduct(upcSku);
				}
				if (ecommerce[0].equals("2")) { // Walmar = 2
					logger.info("WALMART ---------------------------------------" + upcSku);
					modelProdEcom = walmartService.consultProduct(upcSku);
				}
				logger.info(modelProdEcom);
				listProdEcomm.add(modelProdEcom);
			}
		} catch (Exception e) {
			logger.info(
					"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			logger.info(e);
			logger.info(
					"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		logger.info("SALIDA CSVConsult:::" + listProdEcomm);
		return listProdEcomm;
	}
	

}
