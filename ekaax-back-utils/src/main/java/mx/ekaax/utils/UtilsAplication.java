package mx.ekaax.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class UtilsAplication {
	public static void main(String[] args) {
		SpringApplication.run(UtilsAplication.class, args);

	}

}
