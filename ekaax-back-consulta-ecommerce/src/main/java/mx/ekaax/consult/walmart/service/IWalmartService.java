package mx.ekaax.consult.walmart.service;

import mx.ekaax.model.ModelProduct;

public interface IWalmartService {
	
	public ModelProduct consultProduct(String upcSku);

}
