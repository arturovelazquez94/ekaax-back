package mx.ekaax.consult;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableFeignClients("mx.ekaax.consult.feignclient.compare")
@ComponentScan({ "mx.ekaax", "mx.ekaax.generic" })
@EnableDiscoveryClient
public class EkaaxConsultEcommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkaaxConsultEcommerceApplication.class, args);
	}
}
