package mx.ekaax.request;

import mx.ekaax.model.ModelProduct;

public class CompareModelsRequest {

	private ModelProduct modelBase;

	private ModelProduct modelCompare;

	public CompareModelsRequest() {
		super();
	}

	public CompareModelsRequest(ModelProduct modelBase, ModelProduct modelCompare) {
		super();
		this.modelBase = modelBase;
		this.modelCompare = modelCompare;
	}

	public ModelProduct getModelBase() {
		return modelBase;
	}

	public void setModelBase(ModelProduct modelBase) {
		this.modelBase = modelBase;
	}

	public ModelProduct getModelCompare() {
		return modelCompare;
	}

	public void setModelCompare(ModelProduct modelCompare) {
		this.modelCompare = modelCompare;
	}

	@Override
	public String toString() {
		return "CompareModelsRequest [modelBase=" + modelBase + ", modelCompare=" + modelCompare + "]";
	}
	
}
