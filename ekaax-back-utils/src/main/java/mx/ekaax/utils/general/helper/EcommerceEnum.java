package mx.ekaax.utils.general.helper;

public enum EcommerceEnum {

	CHEDRAUI(1, "Chedraui"), WALMART(2, "Walmart"), SUPERAMA(3, "Superama");

	private final int idEcommerce;
	private final String nameEcommerce;

	private EcommerceEnum(int idEcommerce, String nameEcommerce) {
		this.idEcommerce = idEcommerce;
		this.nameEcommerce = nameEcommerce;
	}

	public int getIdEcommerce() {
		return idEcommerce;
	}

	public String getNameEcommerce() {
		return nameEcommerce;
	}

}
