package mx.ekaax.consult.chedraui.service;

import java.util.List;

import mx.ekaax.generic.repository.entity.Product;
import mx.ekaax.model.ModelProduct;

public interface IChedrauiService {
	
	public void create(Product product);
	 
    public void update(Product product);
    
    public ModelProduct consultProduct(String upcSku);
 
    public List <Product> findAll();

}
